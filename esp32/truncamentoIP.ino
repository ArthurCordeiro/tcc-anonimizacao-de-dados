#include <WiFi.h>

const char* ssid     = "Tovarishch";
const char* password = "3lc4rc4ju";
unsigned long start, finished, elapsed;
int matrix[100];
WiFiServer server(80);

void setup()
{
    Serial.begin(115200);
    //pinMode(5, OUTPUT);      // set the LED pin mode

    delay(10);

    // We start by connecting to a WiFi network

    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected.");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    
    server.begin();

}

int value = 0;

void loop(){
 WiFiClient client = server.available();   // listen for incoming clients

  if (client) {                             // if you get a client,
    Serial.println("New Client.");           // print a message out the serial port
    
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        start = millis();
        String req = client.readStringUntil('\r');             // read a byte, then
        Serial.println(req);                    // print it out the serial monitor
     
        String teste = client.remoteIP().toString();
        teste += '\n';
        String s = " ", s1 = " ";
        s += teste, s1 += "IP Truncado: ";
        
        int point = 0;
        int n = teste.length();
        for(int i = 0 ; i < n; i++)
        {
          if(teste[i] == '.')
          {point++;}
          if(point < 2 ){ s1 += teste[i];}
          else {break;}   
          
        }
        s1 += "      .         .    ";
        
        // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
        // and a content-type so the client knows what's coming, then a blank line:
        client.println("HTTP/1.1 200 OK");
        client.println("Content-type:text/html");
        client.println();

        // the content of the HTTP response follows the header:
        client.println(s);
        client.println(s1);

        
        
        
        finished = millis();
        elapsed = finished - start;
        client.println("Millisec: ");
        client.println(elapsed);

        // The HTTP response ends with another blank line:
        client.println();
       
       
       // break out of the while loop:
       break;
        
      }
    }
    // close the connection:
    client.stop();
    Serial.println("Client Disconnected.");
  }
}
