/*
 *  This sketch demonstrates how to set up a simple HTTP-like server.
 *  The server will set a GPIO pin depending on the request
 *    http://server_ip/gpio/0 will set the GPIO2 low,
 *    http://server_ip/gpio/1 will set the GPIO2 high
 *  server_ip is the IP address of the ESP8266 module, will be 
 *  printed to Serial when the module is connected.
 */

 //matrix is generated, does not need input




#include <ESP8266WiFi.h>

const char* ssid = "Tovarishch";
const char* password = "3lc4rc4ju";
unsigned long start, finished, elapsed;
int matrix[11][11];

 
// Create an instance of the server
// specify the port to listen on as an argument
WiFiServer server(80);

void setup() {
  Serial.begin(115200);
  delay(10);

  // prepare GPIO2
  pinMode(2, OUTPUT);
  digitalWrite(2, 0);
  
  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
    
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  
  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.println(WiFi.localIP());
}

void loop() {
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  
  // Wait until the client sends some data
  Serial.println("new client");
  while(!client.available()){
    delay(1);
  }
    
  for(int i = 0, cnt = 1;  i < 10; i++)
  { 
    for(int j = 0; j < 10; j++, cnt++ ) 
    {
      matrix[i][j] = cnt; 
    }

  }
  
  // Read the first line of the request
  start = millis();
  String req = client.readStringUntil('\r');
  Serial.println(req);
  client.flush();

  // Match the request
  //int coluna = int(req[5] - '0');
  //Serial.println(req[5]);
  //Serial.println(int(req[5]));
  //Serial.println(int(req[5]) - int('a'));
  //Serial.println(int('z') - int('a'));
  //Serial.println(req[5]-'0');
  //Serial.println(coluna);
  // Set GPIO2 according to the request
  //digitalWrite(2, val);
  
  client.flush();

  // Prepare the response
  String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\nYou typed: ";
  String s1= "";
  
  int n = req.length();
  Serial.println(req[n - 10]);
  

  for(int i = 0;  i < 10; i++)
  { 
    for(int j = 0; j < 10; j++) 
    {
      s += String(matrix[i][j]);
      s1 += String(matrix[i][j]/10);  
    }

  }
  
  
  s += "</html>\n";
  
  s1 += "\n";
   

  // Send the response to the client
  
  client.print(s);
  client.print(s1);
  
  delay(1);

  
  finished = millis();
  elapsed = finished - start;
  client.print(elapsed);
  
  Serial.println("Client disonnected");

  // The client will actually be disconnected 
  // when the function returns and 'client' object is detroyed
}
