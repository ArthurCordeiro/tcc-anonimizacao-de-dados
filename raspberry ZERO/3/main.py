import string
import time 
from flask import Flask, request
 
app = Flask(__name__)


	
@app.route('/blackmarker/<entrada>')
def black_marker(entrada):
    start_time = time.time()
    variable = ''
    ##for i in entrada:
    ##	variable += 'X'
    for i in range(10000000):
        variable += "a"
    return '{} {} {}'.format(entrada, variable, time.time() - start_time)


@app.route('/substituicao/<entrada>')
def substituicao(entrada):
    start_time = time.time()
    variable = ''
    #for i in entrada:
    #	variable += chr(ord(i) - ord('a') + ord('0'))	
    for i in range(10000000):
        variable += chr(ord('a') - ord('a'))	
    return '{} {} {}'.format(entrada, variable, time.time() - start_time)

@app.route('/generalizacao/<entrada>')
def generalizacao(entrada):
    x = []
    y = []
    var = 0
    #for i in range(10):
    #    y.append(0)
   # for i in range(10):
    #    x.append(y)
    
    #del y
    start_time = time.time()
    s1 = ""
    s2 = ""
    #for i in range(10):
     #   for j in range(10):
      #      x[i][j] = var
       #     var += 1
        #    s1 += str(var)
         #   s2 += str(int(var/10))
       # s1 += "\n"
       # s2 += "\n"
    for i in range(10000):
        for j in range(10000):
            var += 1
            s1 += str(var)
            s2 += str(int(var/10))
    
    return '{} {} {}'.format(s1, s2, time.time() - start_time)

@app.route('/deslocamento/<entrada>')
def deslocamento(entrada):

    x = []
    y = []
                        
    var = 0
   # for i in range(100):
   #     y.append(0)
   # for i in range(10):
   #     x.append(y)
    
   # del y
    start_time = time.time()
    s1 = ""
    s2 = ""
    s2 += ". . "
    #for i in range(10):
    #    for j in range(100):
    #        x[i][j] = var
    #        var += 1
    #        s1 += str(var) + " "
    #        s2 += str(int(var + 10)) + " "
    #    s1 += "\n"
    #    s2 += "\n"
   
    for i in range(10000):
        for j in range(10000):
            var += 1
            #s1 += str(var)
            s2 += str(var + 10)
    return '{} \n {} \n {}'.format(s1, s2, time.time() - start_time)

@app.route('/supressaoatributo/<entrada>')
def supressaoatributo(entrada):
    x = []
    y = []
    entrada = int(entrada) - 1   
    start_time = time.time()
    for i in range(10):
        y.append(1)
    for i in range(10):
        x.append(y)
    del y
    
    s1 = ""
    s2 = ""
   # for i in range(10):
    #    for j in range(10):
    #        s1 += str(x[i][j]) + " "
    #    s1 += "\n"
    #for i in range(10):
    #    x[i][entrada] = 0

    #for i in range(10):
     #   for j in range(10):
      #      s2 += str(x[i][j]) + " "
      #  s2 += "\n"
    for i in range(10000000):
       x[0][0]= 0
    return '{} \n {} \n {}'.format(s1, s2, time.time() - start_time)

@app.route('/truncamentoip/')
def truncamento_ip():
    start_time = time.time()
    s1 = request.remote_addr
    teste = s1.split('.')
    s2 = ""
    for i in range(2):
        s2 += teste[i] + "."
    s2 += "\t."
           
    return '{} \t {} \t {}'.format(s1, s2, time.time() - start_time)  


app.run(host="0.0.0.0", port=8090)
